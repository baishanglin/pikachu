# @Time    : 2020/11/27 5:17 下午
# @Author  : baii
# @File    : 202. 快乐数
# @Use     :
class Solution:
    def isHappy(self, n: int) -> bool:
        def get_next(n: int) -> int:
            total = 0
            for d in str(n):
                total += int(d) ** 2
            return total

        mem = set()
        # 如果 n 不等于1(不快乐) 并且 不在集合中(未形成循环)
        while n != 1 and n not in mem:
            # 这里先添加n是为了防止最原始的n漏掉，会在检测死循环的时候多循环一次
            mem.add(n)
            n = get_next(n)
        return n == 1
