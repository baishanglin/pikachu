# @Time    : 2020/11/20 2:52 下午
# @Author  : baii
# @File    : 24. 两两交换链表中的节点
# @Use     :
# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:

    def swapPairs(self, head: ListNode) -> ListNode:
        dummy_head = ListNode(0)
        dummy_head.next = head   # D -> H
        temp = dummy_head # 指针指向虚拟头结点
        while temp.next and temp.next.next:
            node1 = temp.next  # 第一个元素
            node2 = temp.next.next # 第二个元素
            temp.next = node2 # 由指向第一个元素改为指向第二个元素
            node1.next = node2.next # 第一个元素的指针指向第三个元素
            node2.next = node1 # 第二个元素指向第一个元素 至此交换完成
            temp = node1 # 临时节点 后移两个节点 准备下次置换
        return dummy_head.next